# -*- coding: utf-8 -*-
from robot.libraries.BuiltIn import BuiltIn
import string
import random

locators = {u"input_entry": u"//input[@class='new-todo']",
            u"click_complete": u"//input[@class='toggle' and @type='checkbox']",
            u"entry_completed": u"//li[@class='completed']",
            u"entry_edit": u"//div[@class='view']",
            u"edit_input": u"//input[@class='edit']",
            u"todo_list": u"//ul[@class='todo-list']",
            u"todo_list_rows": u"//li",
            u"number_lefts": u"//span[@class='todo-count']/strong",
            u"filter_complete": u"//a[contains(@href,'completed')]",
            u"clear_completed": u"//button[@class='clear-completed']",
            u"filter_all": u"//a[contains(text(),'All')]",
            u"entry_delete": u"//button[@class='destroy']"
            }


def _seleniumlib():
    return BuiltIn().get_library_instance(u"Selenium2Library")


class ToDoReactPageObject(object):

    @staticmethod
    def check_input(timeout=20):
        _seleniumlib().wait_until_page_contains_element(locators[u"input_entry"], timeout)
        _seleniumlib().wait_until_element_is_visible(locators[u"input_entry"], timeout)
        _seleniumlib().wait_until_element_is_enabled(locators[u"input_entry"], timeout)

    @staticmethod
    def get_random_string(count_symbols=10):
        alphabet = string.ascii_lowercase + string.digits
        return ''.join([random.choice(alphabet) for i in range(count_symbols)])

    @staticmethod
    def input_and_save_name():
        text = ToDoReactPageObject.get_random_string()
        _seleniumlib().input_text(locators[u"input_entry"], text)
        _seleniumlib().press_key(locators[u"input_entry"], u"\\13")
        return text

    @staticmethod
    def check_create_entry(text):
        _seleniumlib().wait_until_page_contains_element(locators[u"entry_edit"], 20)
        _seleniumlib().page_should_contain(text)

    @staticmethod
    def click_delete():
        _seleniumlib().mouse_up(locators[u"entry_edit"])
        _seleniumlib().wait_until_page_contains_element(locators[u"entry_delete"], 20)
        _seleniumlib().click_element(locators[u"entry_delete"])

    @staticmethod
    def check_delete_entry():
        _seleniumlib().wait_until_page_does_not_contain_element(locators[u"entry_edit"], 20)
        _seleniumlib().page_should_not_contain_element(locators[u"entry_edit"])

    @staticmethod
    def edit_entry():
        _seleniumlib().double_click_element(locators[u"entry_edit"] + (u"//label"))
        _seleniumlib().find_element(locators[u"edit_input"]).clear()
        update_text = ToDoReactPageObject.get_random_string()
        _seleniumlib().input_text(locators[u"edit_input"], update_text)
        _seleniumlib().press_key(locators[u"edit_input"], u"\\13")
        return update_text

    @staticmethod
    def complete_entry():
        _seleniumlib().mouse_up(locators[u"click_complete"])
        _seleniumlib().click_element(locators[u"click_complete"])

    @staticmethod
    def check_complete_entry():
        _seleniumlib().wait_until_page_contains_element(locators[u"entry_edit"], 20)
        _seleniumlib().element_should_be_visible(locators[u"entry_completed"])

    @staticmethod
    def click_clear_completed():
        _seleniumlib().click_element(locators[u"clear_completed"])

    @staticmethod
    def check_clear_completed_entry():
        _seleniumlib().element_should_not_be_visible(locators[u"entry_completed"])

    @staticmethod
    def click_clear_completed():
        _seleniumlib().click_element(locators[u"clear_completed"])

    @staticmethod
    def check_counter_on_footer(number):
        rows = _seleniumlib().get_webelements(locators[u"todo_list"] + locators[u"todo_list_rows"])
        count_rows = len(rows)
        numbers_left = _seleniumlib().get_text(locators[u"number_lefts"])
        real = int(numbers_left)
        counted = count_rows - number
        assert real == counted

    @staticmethod
    def exist_elements(locator):
        try:
            _seleniumlib().find_element(locator)
        except:
            return False
        else:
            return True

    @staticmethod
    def check_number_of_completed():
        rows = _seleniumlib().get_webelements(locators[u"todo_list"] + locators[u"todo_list_rows"])
        k = 0
        if len(rows) > 0:
            for i in range(1, len(rows) + 1):
                if ToDoReactPageObject.exist_elements(locators[u"todo_list"] + locators[u"todo_list_rows"]
                                                      + u"[{}]".format(i)+ u"[@class='completed']"):
                    k = k+1
        return k

    @staticmethod
    def check_filter_completed(todo1, todo2):
        _seleniumlib().click_element(locators[u"filter_complete"])
        _seleniumlib().element_should_be_visible(locators[u"entry_edit"] +
                                                 (u"//label[contains(text(),'%s')]" % todo1))
        _seleniumlib().element_should_not_be_visible(locators[u"entry_edit"]
                                                     + (u"//label[contains(text(),'%s')]" % todo2))

    @staticmethod
    def filter_all():
        _seleniumlib().click_element(locators[u"filter_all"])

    @staticmethod
    def delete_all_entries():
        rows = _seleniumlib().get_webelements(locators[u"todo_list"] + locators[u"todo_list_rows"])
        if len(rows) > 0:
            for i in range(1, len(rows) + 1):
                _seleniumlib().mouse_up(locators[u"entry_edit"])
                _seleniumlib().wait_until_page_contains_element(locators[u"entry_delete"], 20)
                _seleniumlib().click_element(locators[u"entry_delete"])
