# -*- coding: utf-8 -*-
*** Settings ***
Library  Selenium2Library  implicit_wait=1.0

Resource  ../Helpers/ToDoReactHelper.robot
Resource  ../BrowserManager.robot

Suite Setup     Prepare Environment
Test Setup      Page Is Open
Test Teardown   Repare Environment
Suite Teardown  Clean Environment


*** Test Cases ***
01 Check Add ToDo
    [Tags]   smoke
    [Documentation]  Проверка добавления записи в список
    Log      Test Case Id = ToDo-1
    ${ToDo1}  Add ToDo
    Check Add ToDo  ${ToDo1}
    Delete ToDo
    Check Delete ToDo

02 Edit ToDo
    [Tags]   smoke
    [Documentation]  Проверка редактирования записи в списке
    Log      Test Case Id = ToDo-2
    ${ToDo1}  Add ToDo
    Check Add ToDo  ${ToDo1}
    ${ToDo1_edit}  Edit ToDo
    Check Edit ToDo  ${ToDo1_edit}
    Delete ToDo
    Check Delete ToDo

03 Check Complete
    [Tags]   smoke
    [Documentation]  Проверка простановки выполнения
    Log      Test Case Id = ToDo-3
    ${ToDo1}  Add ToDo
    Check Add ToDo  ${ToDo1}
    Complete ToDo
    Check Complete ToDo
    Delete ToDo
    Check Delete ToDo

04 Clear Completed
    [Tags]   smoke
    [Documentation]  Удаление исполненных
    Log      Test Case Id = ToDo-4
    ${ToDo1}  Add ToDo
    Check Add ToDo  ${ToDo1}
    Complete ToDo
    Check Complete ToDo
    ${ToDo2}  Add Second ToDo
    Check Add Second ToDo  ${ToDo2}
    Filter Completed ToDo  ${ToDo1}  ${ToDo2}
    Clear Completed
    Check Clear Completed ToDo
    Delete All
    Check Delete All

05 Left Counter
    [Tags]   smoke
    [Documentation]  Проверка работы счетчика неисполненных
    Log      Test Case Id = ToDo-5
    ${ToDo1}  Add ToDo
    Check Add ToDo  ${ToDo1}
    ${ToDo2}  Add Second ToDo
    Check Add Second ToDo  ${ToDo2}
    Complete ToDo
    Check Complete ToDo
    ${Number}  Check Number Completed
    Check Counter  ${Number}
    Delete All
    Check Delete All
