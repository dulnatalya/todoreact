# -*- coding: utf-8 -*-

*** Variables ***
${BROWSER}                      Chrome
${URL}                          http://todomvc.com/examples/react/
${REMOTE_URL}                   http://127.0.0.1:4444/wd/hub
${WINDOWS_SIZE_WIDTH}           1920
${WINDOWS_SIZE_HEIGHT}          1080
