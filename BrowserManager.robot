*** Settings ***
Library  Selenium2Library  implicit_wait=5.0
Library  Helpers.CapturePageSource

Resource  environment.robot

*** Variables ***
${PLATFORM}    Windows
${DESIRED_CAPABILITIES}    {"loggingPrefs": {"driver": "ALL", "server": "ALL", "browser": "ALL"}},  platform: ${PLATFORM}

*** Keywords ***
Prepare Environment
    Open Browser  ${URL}  ${BROWSER}  None  ${REMOTE_URL}  ${DESIRED_CAPABILITIES}
    Set Window Position	0  0
    Set Window Size  ${WINDOWS_SIZE_WIDTH}  ${WINDOWS_SIZE_HEIGHT}

Clean Environment
    Close Browser

Repare Environment
    Capture Log Browser
    Capture Log Driver
    Run Keyword If  '${TEST_STATUS}'=='FAIL'  Run Keywords
        ...  Capture Page Source
        ...  Capture Screenshot And Restart Browser
    Go To  ${URL}

Capture Screenshot And Restart Browser
    Capture Page Screenshot
    Close Browser
    Prepare Environment
